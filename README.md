# Data Persistence And Access #

## Description ##
SQL scripts for superheroDB and console application for data access in a database

## Contributors ##
Karianne Tesaker - https://gitlab.com/kariannet
Stian Selle - https://gitlab.com/stianstian95

## Installation ##
Clone repository and open with Visual Studio.

## SuperheroDB ##
A collection of SQL queries that can be found in the SQLSripts folder. 
The scripts create the database, the tables and relationships between them,
Inserts some dummy data which can then be updated or deleted. Run them in 
sequence.

## Chinook ##
Here we created a console application to interact with the database Chinook. 
The application can manipulate data from database to create objects in C#, 
and with this we create methods to performe simple CRUD operations and some 
more advanced operations specified in the assignment. The files related to 
this can be found in the Models and Repositories folders. We also used the 
repository pattern for achieving our goal.
