﻿USE SuperheroesDB

INSERT INTO Power
(PowerName, PowerDescription)
VALUES('Super strength', 'Near limitless strength')

INSERT INTO Power
(PowerName, PowerDescription)
VALUES('Fly', 'dont need to walk')

INSERT INTO Power
(PowerName, PowerDescription)
VALUES('Laser vision', 'shoots laser beams out of his eyes')

INSERT INTO Power
(PowerName, PowerDescription)
VALUES('Invisibility', 'Can turn invisible to the human eye')

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(1, 1)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(1, 2)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(1, 3)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(1, 4)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(2, 1)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(2, 2)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(2, 3)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(2, 4)

INSERT INTO SuperheroPowers
(SuperheroID, PowerID)
VALUES(3, 4)
