﻿USE SuperheroesDB

INSERT INTO Superhero
(SuperheroName, Alias, Origin)
VALUES('Superman', 'Clark Kent', 'Krypton')

INSERT INTO Superhero
(SuperheroName, Alias, Origin)
VALUES('Captain Marvel', 'Carol Danvers', 'Earth')

INSERT INTO Superhero
(SuperheroName, Alias, Origin)
VALUES('Batman', 'Bruce Wayne', 'Gotham')
