﻿USE SuperheroesDB

CREATE TABLE Superhero (
	ID int PRIMARY KEY IDENTITY(1,1),
	SuperheroName nvarchar(50) NULL,
	Alias nvarchar(50) NULL,
	Origin nvarchar(200) NULL
);

CREATE TABLE Assistant (
	ID int PRIMARY KEY IDENTITY(1,1),
	AssistantName nvarchar(50) NULL
);

CREATE TABLE Power (
	ID int PRIMARY KEY IDENTITY(1,1),
	PowerName nvarchar(50) NULL,
	PowerDescription nvarchar(200) NULL
);