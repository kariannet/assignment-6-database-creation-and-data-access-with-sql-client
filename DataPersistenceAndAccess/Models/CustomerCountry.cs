﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Models
{
    /// <summary>
    /// CustomerCountry class stores the number of customers in the database in this country.
    /// </summary>
    public class CustomerCountry
    {
        public int CustomerNumber { get; set; }
        public string CountryName { get; set; }
    }
}
