﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Models
{
    /// <summary>
    /// CustomerGenre class give how many tracks of this genre this customer has purchased.
    /// Attributes:
    /// - CustomerID: database id for customer entry
    /// - CustomerName: name of the customer
    /// - Genre: name of the genre
    /// - GenreCount: number of tracks of this genre this customer has purchased.
    /// </summary>
    public class CustomerGenre
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Genre { get; set; }
        public int GenreCount { get; set; }
    }
}
