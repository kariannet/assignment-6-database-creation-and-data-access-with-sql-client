﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Models
{
    /// <summary>
    /// CustomerSpender class give total spending by this customer.
    /// </summary>
    public class CustomerSpender
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public decimal TotalSpending { get; set; }
    }
}
