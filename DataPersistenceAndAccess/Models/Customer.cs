﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Models
{
    /// <summary>
    /// Customer class contains following information about a customer:
    /// - Database entry id
    /// - First name and last name
    /// - Country
    /// - Postal code
    /// - Phone number
    /// - Email address
    /// </summary>
    public class Customer
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
