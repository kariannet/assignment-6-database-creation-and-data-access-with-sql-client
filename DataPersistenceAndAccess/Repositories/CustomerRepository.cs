﻿using DataPersistenceAndAccess.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public Customer Get(int elementID)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE CustomerId = @CustomerID";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        cmd.Parameters.AddWithValue("@CustomerId", elementID);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = reader.GetTextReader(1).ReadLine();
                                customer.LastName = reader.GetTextReader(2).ReadLine();
                                customer.Country = reader.GetTextReader(3).ReadLine();
                                customer.PostalCode = reader.GetTextReader(4).ReadLine();
                                customer.PhoneNumber = reader.GetTextReader(5).ReadLine();
                                customer.Email = reader.GetTextReader(6).ReadLine();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customer;
        }

        public Customer GetByName(string firstName, string lastName)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE FirstName LIKE @FirstName AND LastName LIKE @LastName ";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        cmd.Parameters.AddWithValue("@FirstName", '%' + firstName + '%');
                        cmd.Parameters.AddWithValue("@LastName", '%' + lastName + '%');
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = reader.GetTextReader(1).ReadLine();
                                customer.LastName = reader.GetTextReader(2).ReadLine();
                                customer.Country = reader.GetTextReader(3).ReadLine();
                                customer.PostalCode = reader.GetTextReader(4).ReadLine();
                                customer.PhoneNumber = reader.GetTextReader(5).ReadLine();
                                customer.Email = reader.GetTextReader(6).ReadLine();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customer;
        }

        public IEnumerable<Customer> GetAll()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer
                                {
                                    CustomerID = reader.GetInt32(0),
                                    FirstName = reader.GetTextReader(1).ReadLine(),
                                    LastName = reader.GetTextReader(2).ReadLine(),
                                    Country = reader.GetTextReader(3).ReadLine(),
                                    PostalCode = reader.GetTextReader(4).ReadLine(),
                                    PhoneNumber = reader.GetTextReader(5).ReadLine(),
                                    Email = reader.GetTextReader(6).ReadLine()
                                };
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }
        public IEnumerable<Customer> GetLimitedSelected(int limit, int offset)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY ";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer
                                {
                                    CustomerID = reader.GetInt32(0),
                                    FirstName = reader.GetTextReader(1).ReadLine(),
                                    LastName = reader.GetTextReader(2).ReadLine(),
                                    Country = reader.GetTextReader(3).ReadLine(),
                                    PostalCode = reader.GetTextReader(4).ReadLine(),
                                    PhoneNumber = reader.GetTextReader(5).ReadLine(),
                                    Email = reader.GetTextReader(6).ReadLine()
                                };
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        public bool Add(Customer entity)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sqlCmd, connection))
                    {
                        // FirstName, LastName and email can't be null, will be unsuccessful if null
                        command.Parameters.AddWithValue("@FirstName", entity.FirstName);
                        command.Parameters.AddWithValue("@LastName", entity.LastName);
                        command.Parameters.AddWithValue("@Email", entity.Email);

                        // Country, phonenumber and postalcode can be null
                        command.Parameters.AddWithValue("@Country", entity.Country != null ? entity.Country : DBNull.Value);
                        command.Parameters.AddWithValue("@PostalCode", entity.PostalCode != null ? entity.PostalCode : DBNull.Value);
                        command.Parameters.AddWithValue("@Phone", entity.PhoneNumber != null ? entity.PhoneNumber : DBNull.Value);

                        // success
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }

        public bool Update(Customer entity)
        {
            bool success = false;
            string sqlCmd = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                "PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sqlCmd, connection))
                    {
                        // CustomerID, FirstName, LastName and email can't be null, will be unsuccessful if null
                        command.Parameters.AddWithValue("@CustomerId", entity.CustomerID);
                        command.Parameters.AddWithValue("@FirstName", entity.FirstName);
                        command.Parameters.AddWithValue("@LastName", entity.LastName);
                        command.Parameters.AddWithValue("@Email", entity.Email);

                        // Country, phonenumber and postalcode can be null
                        command.Parameters.AddWithValue("@Country", entity.Country != null ? entity.Country : DBNull.Value);
                        command.Parameters.AddWithValue("@PostalCode", entity.PostalCode != null ? entity.PostalCode : DBNull.Value);
                        command.Parameters.AddWithValue("@Phone", entity.PhoneNumber != null ? entity.PhoneNumber : DBNull.Value);

                        // success
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }

        public IEnumerable<CustomerGenre> MostPopularGenre(Customer customer)
        {
            string sqlCmd = "SELECT TOP (1) WITH TIES Customer.CustomerId, Genre.Name AS GenreName, Count(Genre.Name) AS GenreCount " +
                    "FROM Customer " +
                    "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                    "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                    "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                    "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                    "WHERE Customer.CustomerId = @CustomerId " +
                    "GROUP BY Customer.CustomerId, Genre.Name " +
                    "ORDER BY GenreCount DESC";
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        // Reader
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerID);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                CustomerGenre customerGenre = new CustomerGenre
                                {
                                    CustomerID = reader.GetInt32(0),
                                    CustomerName = customer.FirstName + " " + customer.LastName,
                                    Genre = reader.GetTextReader(1).ReadLine(),
                                    GenreCount = reader.GetInt32(2)
                                };

                                customerGenres.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerGenres;
        }

        public IEnumerable<CustomerCountry> NumCustomersInEachCountry()
        {
            string sqlCmd = "SELECT Country, COUNT(*) FROM Customer " +
                "GROUP BY Country ORDER BY COUNT(Country) DESC";
            List<CustomerCountry> customerCountry = new List<CustomerCountry>();

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                CustomerCountry temp = new CustomerCountry
                                {
                                    CustomerNumber = reader.GetInt32(1),
                                    CountryName = reader.GetTextReader(0).ReadLine()
                                };

                                customerCountry.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerCountry;
        }

        public IEnumerable<CustomerSpender> GetCustomerSpenders(int totalNumber)
        {
            string sqlCmd = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) AS 'total' " +
                    "FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                    "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                    "ORDER BY total DESC";
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                CustomerSpender temp = new CustomerSpender
                                {
                                    CustomerID = reader.GetInt32(0),
                                    CustomerName = reader.GetTextReader(1).ReadLine() + " " + reader.GetTextReader(2).ReadLine(),
                                    TotalSpending = reader.GetDecimal(3)
                                };

                                customerSpenders.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerSpenders;
        }
    }
}
