﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Repositories
{
    /// <summary>
    /// Generic Repository Pattern.
    /// </summary>
    /// <typeparam name="T">Type of paramters for the repository pattern.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Get element by id.
        /// </summary>
        /// <param name="elementID">Entry id.</param>
        /// <returns>Element from database.</returns>
        T Get(int elementID);

        /// <summary>
        /// Return all entries.
        /// </summary>
        /// <returns>All entries.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Add entry to table in database.
        /// </summary>
        /// <param name="entity">Element to add</param>
        /// <returns>True if added successfully.</returns>
        bool Add(T entity);

        /// <summary>
        /// Update the entity with the input entity id in the database table.
        /// </summary>
        /// <param name="entity">Entity with updated attributes.</param>
        /// <returns>True if update was successful.</returns>
        bool Update(T entity);

    }
}
