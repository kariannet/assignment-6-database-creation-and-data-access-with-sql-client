﻿using DataPersistenceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistenceAndAccess.Repositories
{
    /// <summary>
    /// Repository pattern for customers for the Chinook database.
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Get customer in database by name.
        /// </summary>
        /// <param name="firstName">First name of customer.</param>
        /// <param name="lastName">Last name of customer.</param>
        /// <returns>Return customer from database.</returns>
        Customer GetByName(string firstName, string lastName);

        /// <summary>
        /// Get a selection of customers of size limit, starting after the given offset.
        /// </summary>
        /// <param name="limit">Limit of selection</param>
        /// <param name="offset">Offset to start selection</param>
        /// <returns>List of customers from database</returns>
        IEnumerable<Customer> GetLimitedSelected(int limit, int offset);

        /// <summary>
        /// List number of customers per country, ordered descendingly.
        /// </summary>
        /// <returns>List of CustomerCountries ordered descendingly.</returns>
        IEnumerable<CustomerCountry> NumCustomersInEachCountry();

        /// <summary>
        /// Get the most popular genre for the given customer (the genre the cutomer has purchased 
        /// most tracks of). In case of ties, all top genres are returned.
        /// </summary>
        /// <param name="customer">Customer to check for.</param>
        /// <returns>List containing most popular CustomerGenre</returns>
        IEnumerable<CustomerGenre> MostPopularGenre(Customer customer);

        /// <summary>
        /// Get the given number of top spending customers.
        /// </summary>
        /// <param name="totalNumber">Number of customers to return.</param>
        /// <returns>List of the top CustomerSpenders.</returns>
        IEnumerable<CustomerSpender> GetCustomerSpenders(int totalNumber);
    }
}
