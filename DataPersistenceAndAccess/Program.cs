﻿using DataPersistenceAndAccess.Models;
using DataPersistenceAndAccess.Repositories;
using System;
using System.Collections.Generic;

namespace DataPersistenceAndAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerRepository repository = new CustomerRepository();

            #region 1. Get all customers
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("All customers in the database:");
            PrintCustomers(repository.GetAll());
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 2. Get customer by identity
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Customer with id=1:");
            PrintCustomer(repository.Get(1));
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 3. Get customer by name:
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Customer with name like 'jack mith':");
            PrintCustomer(repository.GetByName("jack", "mith"));
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 4. Get a limited selection of customers with given offset
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("30 rows of customers starting from row number 6:");
            PrintCustomers(repository.GetLimitedSelected(30, 5));
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 5. Add a new customer to the database
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Adding customer 'Kari Nordmann':");
            Customer customer = new Customer
            {
                FirstName = "Kari",
                LastName = "Nordmann",
                Email = "mail@mail.no",
                Country = "Norway",
                PostalCode = "",
                PhoneNumber = ""
            };
            bool addSuccess = repository.Add(customer);
            Console.WriteLine("Adding 'Kari Nordmann' was successful: " + addSuccess);
            Customer customer2 = repository.GetByName("Kari", "Nordmann");
            PrintCustomer(customer2);
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 6. Update customer
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Updating customer 'Kari Nordmann' to 'Kari Elise Nordmann' with postalcode:");
            customer2.FirstName = "Kari Elise";
            customer2.PostalCode = "4046";
            bool updateSuccess = repository.Update(customer2);
            Console.WriteLine("Updating customer was successful: " + updateSuccess);
            PrintCustomer(repository.Get(customer2.CustomerID));
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 7. Number of customers per country
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Number of customers per country:");
            PrintCustomerCountryList(repository.NumCustomersInEachCountry());
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 8. Get top spenders
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("The top 20 spenders:");
            PrintCustomerSpenders(repository.GetCustomerSpenders(20));
            Console.WriteLine("--------------------------------------------------------------");
            #endregion

            #region 9. Most popular genre
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("The most popular genre(s) for customer with id=12:");
            Customer customer3 = repository.Get(12);
            PrintCustomerGenres(repository.MostPopularGenre(customer3));
            Console.WriteLine("--------------------------------------------------------------");
            #endregion
        }

        /// <summary>
        /// Display all customers.
        /// </summary>
        /// <param name="customers">List of customers to display.</param>
        public static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        /// <summary>
        /// Display attributes of a customer.
        /// </summary>
        /// <param name="customer">Customer to display.</param>
        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- { customer.CustomerID } { customer.FirstName} { customer.LastName } " +
                $"{ customer.Country } { customer.PostalCode } { customer.PhoneNumber } { customer.Email } ---");
        }

        /// <summary>
        /// Display number of customers per country in descending order.
        /// </summary>
        /// <param name="customerCountries">List to display.</param>
        public static void PrintCustomerCountryList(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                PrintCustomerCountry(customerCountry);
            }
        }

        /// <summary>
        /// Display attributes of a CustomerCountry.
        /// </summary>
        /// <param name="customerCountry">CustomerCountry to display</param>
        public static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"--- { customerCountry.CountryName } { customerCountry.CustomerNumber} ---");
        }

        /// <summary>
        /// Display list of CustomerSpenders.
        /// </summary>
        /// <param name="customerSpenders">List to display</param>
        public static void PrintCustomerSpenders(IEnumerable<CustomerSpender> customerSpenders)
        {
            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                PrintCustomerSpender(customerSpender);
            }
        }

        /// <summary>
        /// Display attributes of a CustomerSpender.
        /// </summary>
        /// <param name="customerSpender">CustomerSpender to display</param>
        public static void PrintCustomerSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"--- { customerSpender.CustomerID } { customerSpender.CustomerName} { customerSpender.TotalSpending } ---");
        }

        /// <summary>
        /// Display list of CustomerGenres.
        /// </summary>
        /// <param name="customerGenres">List to display</param>
        public static void PrintCustomerGenres(IEnumerable<CustomerGenre> customerGenres)
        {
            foreach (CustomerGenre customerGenre in customerGenres)
            {
                PrintCustomerGenre(customerGenre);
            }
        }

        /// <summary>
        /// Display attributes of a CustomerGenre.
        /// </summary>
        /// <param name="customerGenre">CustomerGenre to display.</param>
        public static void PrintCustomerGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"--- { customerGenre.CustomerID } { customerGenre.CustomerName} { customerGenre.Genre } { customerGenre.GenreCount }  ---");
        }
    }
}
